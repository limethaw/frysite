import os, json
from flask import Flask, render_template, request, redirect, url_for, flash
from subprocess import call, check_output

# System parameters

# The folder where the input and output images are stored
UPLOAD_FOLDER = "static/images/"
# The accepted formats for input files
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
# How many images we want to store before deleting old ones
MAX_IMAGES=1024

# Init server
app = Flask("Frysite")
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['CNTR'] = 0

# Helper function to check for allowed file formats
def allowed_file(filename):
	return '.' in filename and \
		   filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# Main page for image upload
@app.route("/")
def index():
	return render_template("frysite.html")

# Upload path, processes image and reroutes to show
@app.route("/upload/", methods=["POST"])
def upload():

	# Check if there is a file attached
	if 'image' not in request.files:
		flash('No file part')
		return redirect(request.url)

	# Get the image
	image = request.files["image"]

	# Check if the uploaded file is empty
	if image.filename == "":
		flash('No file part')
		return redirect(request.url)

	if image and allowed_file(image.filename):
		# Save it with a number as filename to enforce overwriting
		scntr = str(app.config['CNTR'])
		app.config['CNTR'] = (app.config['CNTR']+1) % MAX_IMAGES
		ext = image.filename.rsplit('.', 1)[1].lower()
		image.save(os.path.join(app.config['UPLOAD_FOLDER'], scntr+"."+ext))

		# Deepfry it with the script
		call(["./fry.py", UPLOAD_FOLDER+scntr+"."+ext])

		# Redirect to show the output image
		return redirect(url_for("showFile", filenumber=scntr+"."+ext))

@app.route("/update/")
def update():
	filename = request.args.get("file", "static/images/test.png");
	outfile, e = os.path.splitext(filename)
	outfile = outfile + "_fried.jpg"

	params = {}
	params["saturation"] = request.args.get("saturation", 1);
	params["contrast"] = request.args.get("contrast", 1);
	params["brightness"] = request.args.get("brightness", 1);
	params["noiseFactor"] = request.args.get("noiseFactor", 1);
	params["chunkFactor"] = request.args.get("chunkFactor", 0);
	params["iterations"] = request.args.get("iterations", 1);
	params["quality"] = request.args.get("quality", 75);

	params["bulges"] = request.args.get("bulges", "").rsplit(";")
	if params["bulges"][0] == "":
		params["bulges"] = []
	params["swirls"] = request.args.get("swirls", "").rsplit(";")
	print(params["swirls"])
	if params["swirls"][0] == "":
		params["swirls"] = []

	print(check_output(["./fry.py", filename, json.dumps(params)]))

	#return "/static/images/1.jpg"
	return outfile

# Show path to see the fried image and upload another one
@app.route("/show/<filenumber>")
def showFile(filenumber):
	basis, e = os.path.splitext(filenumber)
	print(filenumber)
	return render_template("frysite.html", filename="/"+UPLOAD_FOLDER+basis+"_fried.jpg", oldfile=UPLOAD_FOLDER+filenumber)

@app.after_request
def add_header(r):
	r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
	r.headers["Pragma"] = "no-cache"
	r.headers["Expires"] = "0"
	r.headers['Cache-Control'] = 'public, max-age=0'
	return r
