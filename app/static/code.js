var saturation, contrast, brightness, iterations, quality, image, can, ctx;

var bulges = [];
var swirls = [];

var dotSize = 10;

function onload(){
	saturation = document.getElementById("saturation");
	contrast = document.getElementById("contrast");
	brightness = document.getElementById("brightness");
	noiseFactor = document.getElementById("noiseFactor");
	chunkFactor = document.getElementById("chunkFactor");
	iterations = document.getElementById("iterations");
	quality = document.getElementById("quality");
	image = document.getElementById("fried-image");

	can = document.getElementById("bulge-can");
	ctx = can.getContext("2d");
	resizeCan();

	window.addEventListener("wheel", resizeFeature);
}

function drawFeatures() {

		function lerp(a, b, t) {
				return a+t*(b-a);
		}

	ctx.clearRect(0, 0, can.width, can.height);
	var scale = [image.naturalWidth / image.width];

	for(i in bulges) {
		var pos = {x: bulges[i].x/scale, y: bulges[i].y/scale};

		ctx.beginPath();
		ctx.arc(pos.x, pos.y, dotSize, 0, 2 * Math.PI, false);
		ctx.fillStyle = 'white';
		ctx.fill();
		ctx.lineWidth = 3;
		ctx.strokeStyle = 'black';
		ctx.stroke();

		ctx.beginPath();
		ctx.arc(pos.x, pos.y, bulges[i].r/scale, 0, 2 * Math.PI, false);
		ctx.lineWidth = 1;
		ctx.strokeStyle = 'white';
		ctx.stroke();

		ctx.beginPath();
				var f = bulges[i].s;
				if(f>0) {
						f = lerp(0.1*bulges[i].r/scale, bulges[i].r/scale, f);
				ctx.strokeStyle = 'green';
				} else {
						f = lerp(0.1*bulges[i].r/scale, bulges[i].r/scale, -f);
				ctx.strokeStyle = 'red';
				}
		ctx.arc(pos.x, pos.y, f, 0, 2 * Math.PI, false);
		ctx.lineWidth = 1;
		ctx.stroke();
	}

	for(i in swirls) {
		var pos = {x: swirls[i].x/scale, y: swirls[i].y/scale};

		ctx.beginPath();
		ctx.arc(pos.x, pos.y, dotSize, 0, 2 * Math.PI, false);
		ctx.fillStyle = 'black';
		ctx.fill();
		ctx.lineWidth = 3;
		ctx.strokeStyle = 'white';
		ctx.stroke();

		ctx.beginPath();
		ctx.arc(pos.x, pos.y, swirls[i].r/scale, 0, 2 * Math.PI, false);
		ctx.lineWidth = 1;
		ctx.strokeStyle = 'white';
		ctx.stroke();

		ctx.beginPath();
				var f = swirls[i].s;
				if(f>0) {
						f = lerp(0.1*swirls[i].r/scale, swirls[i].r/scale, f);
				ctx.strokeStyle = 'green';
				} else {
						f = lerp(0.1*swirls[i].r/scale, swirls[i].r/scale, -f);
				ctx.strokeStyle = 'red';
				}
		ctx.arc(pos.x, pos.y, f, 0, 2 * Math.PI, false);
		ctx.lineWidth = 1;
		ctx.stroke();
	}
}

function resizeFeature(e) {

	function norm(a, b) {
		return Math.sqrt((a.x-b.x)**2 + (a.y-b.y)**2)
	}

	var pos = getMousePos(e);
	var scale = [image.naturalWidth / image.width];
	pos = {x: pos.x*scale, y: pos.y*scale};

	for(i in bulges) {
		if(norm(pos, bulges[i]) < dotSize) {
			if(!e.ctrlKey) {
				bulges[i].r -= Math.sign(e.deltaY) * 10;
			} else {
				bulges[i].s -= Math.sign(e.deltaY) * 0.1;
				bulges[i].s = Math.max(-1, Math.min(bulges[i].s, 1))
			}
			drawFeatures();
			e.preventDefault();
						return;
		}
	}

	for(i in swirls) {
		if(norm(pos, swirls[i]) < dotSize) {
			if(!e.ctrlKey) {
				swirls[i].r -= Math.sign(e.deltaY) * 10;
			} else {
				swirls[i].s -= Math.sign(e.deltaY) * 0.1;
				swirls[i].s = Math.max(-1, Math.min(swirls[i].s, 1))
			}
			drawFeatures();
			e.preventDefault();
						return;
		}
	}
}

function refresh() {

	var bulgeString = ""
	for(i in bulges) {
		b = bulges[i];
		if(i>0) bulgeString += ";";
		bulgeString += String(Math.round(b.x))+":"+String(Math.round(b.y))+":"+String(b.r)+":"+String(b.s);
	}

	var swirlString = ""
	for(i in swirls) {
		console.log(swirls[i]);
		b = swirls[i];
		if(i>0) swirlString += ";";
		swirlString += String(Math.round(b.x))+":"+String(Math.round(b.y))+":"+String(b.r)+":"+String(b.s);
	}

	var xhttp = new XMLHttpRequest();
	var requestString = "/update/?file="+image.alt+
		"&saturation="+saturation.value+
		"&contrast="+contrast.value+
		"&brightness="+brightness.value+
		"&noiseFactor="+noiseFactor.value+
		"&chunkFactor="+chunkFactor.value+
		"&iterations=1"+
		"&quality="+quality.value;
	if(bulges.length>0) requestString += "&bulges="+bulgeString;
	if(swirls.length>0) requestString += "&swirls="+swirlString;
	$("#state").html("Waiting...");
	$("#state").css("color", "red");
	xhttp.open("GET", requestString, true);

	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			image.src = "/" + xhttp.responseText+"?token="+new Date();
			$("#state").html("Ready");
			$("#state").css("color", "green");
		}
	}

	xhttp.send();
}

function resizeCan() {
	can.width = image.width;
	can.height = image.height;
}

function  getMousePos(evt) {
  var rect = can.getBoundingClientRect();
  return {
	x: (evt.clientX - rect.left),
	y: (evt.clientY - rect.top)
  }
}

function placeFeature(evt) {
	if(evt.altKey) {
		placeSwirl(evt);
	} else {
		placeBulge(evt);
	}
	drawFeatures();
}

function placeBulge(evt) {
	var pos = getMousePos(evt);

	// Assuming uniform scale
	var scale = [image.naturalWidth / image.width];
	bulges.push({x: pos.x*scale, y: pos.y*scale, r: 100, s: 0.5});
}

function placeSwirl(evt) {
	var pos = getMousePos(evt);

	// Assuming uniform scale
	var scale = [image.naturalWidth / image.width];
	swirls.push({x: pos.x*scale, y: pos.y*scale, r: 100, s: 0.5});
}
