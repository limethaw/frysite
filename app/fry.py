#! /usr/bin/env python3

import os, sys, numpy, json
from PIL import Image, ImageFilter, ImageEnhance
from random import uniform, randint
from math import sqrt, cos
from time import sleep
PI = 3.141593

# Parameters for frying
# Given by the website
saturationFactor = 5
contrastFactor = 5
brightnessFactor = 1
noiseFactor = 0
chunkFactor = 0.5 # Must be between 0 and 1
jpegIterations = 1
jpegQuality = 75
bulges = []
swirls = []

# Should be given, currently constant
edgeEnhancements = 3
noiseMaximization = 5

# Unused as of now
numEmojis = 3

# The path to the emojis, and a list of all emoji filenames to choose from
emojiPath = "static/emojis/"
emojiNames = [
	"angery.png",
	"dab.png",
	"laughing.png",
	"thonking.png",
	"100.png",
]



# Adds a bulge to the image img centered at the point given as 2-tuple with the given radius.
# Limits zoom factor to the given value, which should be in (0, 1) with lower values for higher distortion.
def bulge2(img, center, radius, max_distort):
	arr = numpy.asarray(img);
	newarr = numpy.empty(arr.shape, dtype=arr.dtype);

	ys, xs = numpy.mgrid[0:arr.shape[0], 0:arr.shape[1]]
	ds = numpy.sqrt((xs-center[1])**2 + (ys-center[0])**2)

	fac = 1-max_distort+max_distort*(ds/radius)
	if max_distort > 0:
		fac[fac>1]=1
	else:
		fac[fac<1]=1

	dx = numpy.array(center[1]+fac*(xs-center[1]), dtype=numpy.int32)
	dx = numpy.clip(dx, 0, arr.shape[1]-1)

	dy = numpy.array(center[0]+fac*(ys-center[0]), dtype=numpy.int32)
	dy = numpy.clip(dy, 0, arr.shape[0]-1)

	newarr = arr[dy, dx]

	return Image.fromarray(newarr)

def swirl(img, center, radius, max_distort):
	max_distort *= PI
	arr = numpy.asarray(img);
	newarr = numpy.empty(arr.shape, dtype=arr.dtype);

	ys, xs = numpy.mgrid[0:arr.shape[0], 0:arr.shape[1]]
	rs = numpy.sqrt((xs-center[1])**2 + (ys-center[0])**2)
	phis = numpy.arctan2(ys-center[0], xs-center[1])

	fac = max_distort*(1-2*numpy.abs(rs/radius-0.5))
	if max_distort > 0:
		fac[fac<0]=0
	else:
		fac[fac>0]=0

	dphis = phis+fac
	dx = numpy.array(center[1]+rs*numpy.cos(dphis), dtype=numpy.int32)
	dx = numpy.clip(dx, 0, arr.shape[1]-1)
	dy = numpy.array(center[0]+rs*numpy.sin(dphis), dtype=numpy.int32)
	dy = numpy.clip(dy, 0, arr.shape[1]-1)

	newarr = arr[dy, dx]
	return Image.fromarray(newarr)

# Adds a normal distributed pixel-grained noise to a PIL image
def noisy(img):
	bands = len(img.getbands())
	noise = numpy.random.normal(0, noiseFactor, img.size[0]*img.size[1]*bands)
	noise = numpy.reshape(noise, (img.size[1], img.size[0], bands))
	ret = numpy.uint8(numpy.add(noise, numpy.asarray(img)).clip(0, 255))
	return Image.fromarray(ret)

# Adds pixel grained and larger grained noise to an image by minimizing, adding noise, magnifying
# and adding it to the original image with noise
# Badly calibrated and currently unused
def convoluted(img):
	smolSize = tuple(int(x/noiseMaximization) for x in img.size)
	smolimg = img.copy();
	smolimg.thumbnail(smolSize, Image.NEAREST)
	smolimg = noisy(smolimg).resize(img.size, Image.NEAREST)
	return Image.blend(noisy(img), noisy(smolimg), chunkFactor)



# Main script



# Checking input parameters
if len(sys.argv)<2:
	raise ValueError("Need to provide image path")
infile = sys.argv[1]

if len(sys.argv)>2:
	params = json.loads(sys.argv[2])

	if "saturation" in params: saturationFactor = float(params["saturation"])
	if "contrast" in params: contrastFactor = float(params["contrast"])
	if "brightness" in params: brightnessFactor = float(params["brightness"])
	if "noiseFactor" in params: noiseFactor = float(params["noiseFactor"])
	if "chunkFactor" in params: chunkFactor = float(params["chunkFactor"])
	if "iterations" in params: jpegIterations = int(params["iterations"])
	if "quality" in params: jpegQuality = int(params["quality"])
	if "bulges" in params:
		bulges = []
		for b in params["bulges"]:
			x, y, r, s = b.rsplit(':', 3)
			bulges.append((int(y), int(x), float(r), float(s)))
	if "swirls" in params:
		swirls = []
		for b in params["swirls"]:
			x, y, r, s = b.rsplit(':', 3)
			swirls.append((int(y), int(x), float(r), float(s)))



# Load original image
f, e = os.path.splitext(infile)
outfile = f + "_fried.jpg"
if infile != outfile:
	img = Image.open(infile).convert("RGB")
	img.load()

	# Add random emojis
	#img = emojify(img, numEmojis)

	img = convoluted(img)

	# Add contrast and saturation
	enhancer = ImageEnhance.Brightness(img)
	img = enhancer.enhance(brightnessFactor)
	enhancer = ImageEnhance.Contrast(img)
	img = enhancer.enhance(contrastFactor)
	enhancer = ImageEnhance.Color(img)
	img = enhancer.enhance(saturationFactor)

	# Add swirls
	for i in range(len(swirls)):
		position = (swirls[i][0], swirls[i][1])
		radius = swirls[i][2]
		strength = swirls[i][3]
		img = swirl(img, position, radius, strength)

	# Add bulges
	for i in range(len(bulges)):
		position = (bulges[i][0], bulges[i][1])
		radius = bulges[i][2]
		strength = bulges[i][3]
		img = bulge2(img, position, radius, strength)

	# Enhance edges
	"""for i in range(edgeEnhancements):
		print("e"+str(i))
		img = img.filter(ImageFilter.SHARPEN)
		#img = img.filter(ImageFilter.FIND_EDGES)
		pass"""

	# Safe output
	print(".")
	img.save(outfile)
	print(".")

# JPEG compression with terrible quality (minimum is 1, normal is 75)
for i in range(jpegIterations):
	print("c"+str(i))
	sleep(0.1)
	img = Image.open(outfile)
	img.save(outfile, quality=jpegQuality)



# UNUSED FUNCTIONS ==================================================================================

# Selects random emojis and pastes them onto the image with random locations and rotations.
# The number of emojis is given as parameter
def emojify(img, numEmo):
	if numEmo > 0:
		pasteimg = Image.new("RGBA", img.size)
		for i in range(numEmo):
			index = randint(0, len(emojiNames)-1)
			em = Image.open(emojiPath + emojiNames[index])
			em = em.rotate(uniform(-90, 90), expand=True)
			print((img.size[0]-em.size[0], img.size[1]-em.size[1]))
			pos = (randint(0, img.size[0]-em.size[0]), randint(0, img.size[1]-em.size[1]))
			pasteimg.paste(em, pos)
		return Image.alpha_composite(img, pasteimg)
